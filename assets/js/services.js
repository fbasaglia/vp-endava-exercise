(function(){
	'use strict';

	App.factory( 'Services', Services );

	function Services( $http ) {
		var self = this;

		self.getUsersData = getUsersData;

		function getUsersData() {
			return $http.get('/assets/json/users.json').then( function ( response ){

				return response.data;

			});
		}
		
		return self;
	}

})();