(function(){
	'use strict';

	App.component( 'vpApp', {
		controller: VpAppController,
		templateUrl: '/assets/templates/vp-app.html'
	});

	VpAppController.$inject = [ '$scope', 'Services' ];
	
	function VpAppController( $scope, Services ) {
		var self = this;

		self.$onInit = onInit;
		self.usersData;

		function onInit() {
			Services.getUsersData().then( 
				function ( response ) {
					
					self.usersData = response;
					$scope.$broadcast( 'initSearchbox' );
					
				},
				function () {
					
					$scope.$broadcast( 'usersServiceError' );

				}
			);
		}

	}

})();