(function(){
	'use strict';

	App.component( 'searchBox', {
		require: {
			mainCtrl: '^vpApp'
		},
		controller: SearchBoxController,
		templateUrl: '/assets/templates/search-box.html'
	});
	
	SearchBoxController.$inject = [ '$scope' ];

	function SearchBoxController( $scope ) {
		var self = this;

		/** Methods */
		self.$onInit = onInit;
		self.onSearchTrigger = onSearchTrigger;
		self.onSearchboxChange = onSearchboxChange;
		self.forceSearchTrigger = forceSearchTrigger;

		/** Variables */
		self.userName = '';
		self.searchResult;
		self.searchedUserName;
		self.searchSuggestions;
		
		function onInit () {
			$scope.$on( 'initSearchbox', function() {

				self.usersDataLoaded = true;

			});

			$scope.$on( 'usersServiceError', function() {
				
				self.usersServiceError = true;
				
			});
		}

		function onSearchTrigger() {

			self.searchSuggestions = [];
			self.searchedUserName = self.userName;
			self.searchResult = self.mainCtrl.usersData.filter( filterUsersData );

		}

		function forceSearchTrigger( value ) {
			
			self.userName = value;
			onSearchTrigger();

		}
		
		function onSearchboxChange() {

			self.searchSuggestions = self.mainCtrl.usersData.filter( filterUsersData );
		
		}

		function filterUsersData( item ) {
			if ( self.userName && item.username.indexOf( self.userName ) !== -1 ) 
				return item;
		}
	}

})();