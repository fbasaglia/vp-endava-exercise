(function(){
	'use strict';

	App.component( 'resultBox', {
		require: {
			searchCtrl: '^searchBox'
		},
		controller: ResultBoxController,
		templateUrl: '/assets/templates/result-box.html'
	});

	function ResultBoxController() {
		var self = this;

		/** Methos */
		self.viewDetails = viewDetails;
		self.closeDetails = closeDetails;

		/** Variables */
		self.showDetails = false;
		self.detailToShow;

		function viewDetails( item ) {
			
			self.showDetails = true;
			self.detailToShow = item;

		}
		
		function closeDetails() {
			
			self.showDetails = false;
			self.detailToShow = null;

		}
	}
})();