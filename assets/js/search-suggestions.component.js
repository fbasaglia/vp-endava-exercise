(function(){
	'use strict';

	App.component( 'searchSuggestions', {
		require: {
			searchCtrl: '^searchBox'
		},
		controller: SearchSuggestionsController,
		templateUrl: '/assets/templates/search-suggestions.html'
	});

	function SearchSuggestionsController() {
		var self = this;

		/** Methos */
		self.triggerSearch = triggerSearch;

		function triggerSearch( value ) {
			self.searchCtrl.forceSearchTrigger( value );
		}
	}
})();